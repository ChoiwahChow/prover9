# Introduction

This is source code for prover9 and mace4....

## Build
To build prover9 and mace4, issue the following commands

    cd build && cmake ..
    make

That is, go to the build directory, use cmake to make the Makefiles,
and do the make of both prover9 and mace4.

To build just prover9, do

    make prover9


Similarly, to build just mace4, do

    make mace4


To get better performance, use the compiler flags `-O3` and `NDEBUG`.  This can be done easily with the Release build type.  That is, instead of just `cmake ..` as described above, do

    cmake -DCMAKE_BUILD_TYPE=Release ..
    
The optimized version is often more than twice as fast as the non-optimized version.